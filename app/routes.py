from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Vahid'}
    posts = [
        {
            'name': {'username': 'ServiceDesk'},
            'body': '2020'
        },
        {
            'name': {'username': 'SD Supervisor'},
            'body': '2025'
        }
    ]
    return render_template('index.html', title='Home', user=user, posts=posts)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 13:24:04 2024

@author: imto1
"""

from flask import Flask

app = Flask(__name__)

from app import routes
# Libre Phonebook
Free(as in freedom) phonebook web application.

I started this project to learn [Flask](https://flask.palletsprojects.com/en/3.0.x/) microframework. I will use [The Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) by [Miguel Grinberg](https://mstdn.social/@miguelgrinberg).